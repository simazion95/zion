import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'},{title:'Gone For Good', author:'Harlen Coben'}]; 

getBooks(){
  const booksObservable = new Observable(
    observer => {
      setInterval(
        ()=>observer.next(this.books),5000
      )
    }
  )
  return booksObservable;
}

addBooks(){
  setInterval(
    () => this.books.push({title:'A new book', author:'New author'})
    ,5000)
}
  /*
  getBooks(){
    setInterval(()=> this.books ,1000);
  }
*/
  constructor() { }
}