import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

   
 

  likes = 0;
  

  constructor(private route: ActivatedRoute) { 
   
  }
  temperature; 
  city;

  addLikes(){
    this.likes++
  }
  ngOnInit() {
    this.temperature = this.route.snapshot.params.temp;
    this.city= this.route.snapshot.params.city;
  }

}
