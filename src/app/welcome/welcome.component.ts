import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {



  title:string = "Welcome";
  panelOpenState=false;
  //books:any;
  books$:Observable<any>;

  constructor(private booksservise:BooksService) { }

  ngOnInit() {
    /*
    this.books=this.booksservise.getBooks().subscribe(
      (books)=>this.books = books
    );
    */
   this.booksservise.addBooks();
   this.books$=this.booksservise.getBooks();
  }

}
